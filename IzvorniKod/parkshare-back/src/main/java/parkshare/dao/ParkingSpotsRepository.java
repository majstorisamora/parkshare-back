package parkshare.dao;



import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import parkshare.domain.ParkingSpot;

@Repository
public interface ParkingSpotsRepository  extends JpaRepository<ParkingSpot,Long> {
}
