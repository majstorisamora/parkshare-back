package parkshare.dao;

import java.util.List;
import java.util.Optional;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import parkshare.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    Optional<User> findByUsername(String username);
    Optional<User> findByEmail(String email);
    Optional<User> findById(String id);
    int countByUsername(String username);
    int countByEmail(String email);

    @Transactional
    @Modifying
    @Query("UPDATE users a "+ "SET a.enabled= 2 WHERE a.username=?1")
    int enableUser(String username);

    @Transactional
    @Modifying
    @Query("UPDATE users a "+ "SET a.enabled= 1 WHERE a.username=?1")
    int askAdminConfirmation(String username);

    @Query("SELECT u FROM users u WHERE u.enabled=1")
    List<User> findAllOwnersUnconfirmedByAdmin();

    @Transactional
    @Modifying
    @Query("UPDATE users a "+ "SET a.idParking= ?2 " + "WHERE a.username=?1")
    int updateUserParking(String username, Long parkingId);
}
