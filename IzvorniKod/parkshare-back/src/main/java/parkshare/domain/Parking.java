package parkshare.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Type;

@Entity
public class Parking {

    @Id
    @GeneratedValue
    private Long id_parking;

    @Column(unique=true,nullable=false)
    private String name;

    @Column
    private String description;

    @Column(nullable = false)
    private Float price;

    @Column(nullable = false)
    private String type;

    @Lob
    @Type(type="org.hibernate.type.BinaryType")
    @Column
    private byte[] image;

    @Column(nullable = false)
    private Integer number_of_spots;

    @Column(nullable = false)
    private String location;

    @OneToOne
    @JoinColumn(nullable = false,name="owner_id")
    private User owner;

    public Long getId_parking() {
        return this.id_parking;
    }

    public void setId_parking(Long id_parking) {
        this.id_parking = id_parking;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return this.price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getImage() {
        return this.image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Integer getNumber_of_spots() {
        return this.number_of_spots;
    }

    public void setNumber_of_spots(Integer number_of_spots) {
        this.number_of_spots = number_of_spots;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location=location;
    }

    public Parking(String name, String description, Float price, String type, byte[] image,
                   Integer number_of_spots, User owner, String location) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.type = type;
        this.image = image;
        this.number_of_spots = number_of_spots;
        this.owner = owner;
        this.location = location;
    }
    public Parking() {
    }

}
