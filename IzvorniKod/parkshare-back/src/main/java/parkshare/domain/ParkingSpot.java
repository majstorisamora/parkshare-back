package parkshare.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ParkingSpot {

    @Id
    @GeneratedValue
    private Long id_parkingSpot;

    @Column(nullable = false)
    private Boolean reservation;

    @Column(nullable = false)
    private Boolean taken;

    @Column(nullable = false)
    private String location1;

    @Column(nullable = false)
    private String location2;

    @Column(nullable = false)
    private String location3;

    @Column(nullable = false)
    private String location4;

    @ManyToOne
    @JoinColumn(nullable = false,name="parking_id")
    private Parking parking;

    public Long getId_parkingSpot() {
        return this.id_parkingSpot;
    }

    public void setId_parkingSpot(Long id_parkingSpot) {
        this.id_parkingSpot = id_parkingSpot;
    }

    public Boolean isReservation() {
        return this.reservation;
    }

    public Boolean getReservation() {
        return this.reservation;
    }

    public void setReservation(Boolean reservation) {
        this.reservation = reservation;
    }

    public Boolean isTaken() {
        return this.taken;
    }

    public Boolean getTaken() {
        return this.taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }

    public String getLocation1() {
        return this.location1;
    }

    public void setLocation1(String location1) {
        this.location1 = location1;
    }

    public String getLocation2() {
        return this.location2;
    }

    public void setLocation2(String location2) {
        this.location2 = location2;
    }

    public String getLocation3() {
        return this.location3;
    }

    public void setLocation3(String location3) {
        this.location3 = location3;
    }

    public String getLocation4() {
        return this.location4;
    }

    public void setLocation4(String location4) {
        this.location4 = location4;
    }

    public Parking getParking() {
        return this.parking;
    }

    public void setParking(Parking parking) {
        this.parking = parking;
    }

    public ParkingSpot(Boolean reservation, Boolean taken, String location1, String location2, String location3, String location4, Parking parking) {
        this.reservation = reservation;
        this.taken = taken;
        this.location1 = location1;
        this.location2 = location2;
        this.location3 = location3;
        this.location4 = location4;
        this.parking = parking;
    }

    public ParkingSpot() {
    }


    
}
