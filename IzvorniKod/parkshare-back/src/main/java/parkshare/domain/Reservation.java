package parkshare.domain;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Reservation {

    @Id
    @GeneratedValue
    private Long id_reservation;

    @Column(nullable = false)
    private Boolean repetitive;

    @Column(nullable = false)
    private LocalDateTime reservation_start;

    @Column(nullable = false)
    private LocalDateTime reservation_end;

    @ManyToOne
    @JoinColumn(nullable = false,name="client_id")
    private User client;

    @ManyToOne
    @JoinColumn(nullable = false,name="parkingSpot_id")
    private ParkingSpot parkingSpot;

    public Reservation(Boolean repetitive, LocalDateTime reservation_start,
                       LocalDateTime reservation_end, User client, ParkingSpot parkingSpot) {
        this.repetitive = repetitive;
        this.reservation_start = reservation_start;
        this.reservation_end = reservation_end;
        this.client = client;
        this.parkingSpot = parkingSpot;
    }

    public Reservation(){}

    public Long getId_reservation() {
        return id_reservation;
    }

    public void setId_reservation(Long id_reservation) {
        this.id_reservation = id_reservation;
    }

    public Boolean getRepetitive() {
        return repetitive;
    }

    public void setRepetitive(Boolean repetitive) {
        this.repetitive = repetitive;
    }

    public LocalDateTime getReservation_start() {
        return reservation_start;
    }

    public void setReservation_start(LocalDateTime reservation_start) {
        this.reservation_start = reservation_start;
    }

    public LocalDateTime getReservation_end() {
        return reservation_end;
    }

    public void setReservation_end(LocalDateTime reservation_end) {
        this.reservation_end = reservation_end;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public ParkingSpot getParkingSpot() {
        return parkingSpot;
    }

    public void setParkingSpot(ParkingSpot parkingSpot) {
        this.parkingSpot = parkingSpot;
    }
}
