package parkshare.domain;


import org.hibernate.annotations.Type;

import java.util.*;

import javax.persistence.*;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name="users")
public class User  {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false,unique = true)
    private String username;
    @Column(nullable = false,unique = true)
    private String email;


    @Lob
    @Type(type="org.hibernate.type.BinaryType")
    @Column
    private byte[] imageUrl;//promjeniti u bytea ili lob(ili je to blob koji se ne smije ili large object notation)
    // @Column
    // private String imageUrl;

    @Column(nullable = false)
    @Size(min=0,max=99)
    private String iban;
    @Column(nullable = false)
    private String password;
    @Enumerated(EnumType.STRING)
    private UserRole userRole;
    //0-unconfirmeed 1-uconfirmedOwner 2-confirmed
    @Column
    private Integer enabled;
    @Column
    private Float wallet;
    @Column
    private Long idParking;
    @Column
    private Boolean wallet_enabled;
    @Column
    private String numOfCreditCard;

    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL)
    @JsonIgnore
    private List<ConfirmationToken> tokens=new ArrayList<>();

    public User(String firstName, String lastName, String username, String password, String email, byte[] imageUrl, String iban, UserRole userRole, Integer enabled,Boolean wallet_enabled, Float wallet, Long idParking, String numOfCreditCard) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.imageUrl = imageUrl;
        this.iban = iban;
        this.password = password;
        this.userRole = userRole;
        this.enabled = enabled;
        this.wallet = wallet;
        this.idParking = idParking;
        this.wallet_enabled = wallet_enabled;
        this.numOfCreditCard = numOfCreditCard;
    }

    // public User(String firstName, String lastName, String username, String password, String email, String imageUrl, String iban, UserRole userRole, Integer enabled, Boolean wallet_enabled,Float wallet, Long idParking, String numOfCreditCard) {
    //     this.firstName = firstName;
    //     this.lastName = lastName;
    //     this.username = username;
    //     this.email = email;
    //     this.imageUrl = imageUrl;
    //     this.iban = iban;
    //     this.password = password;
    //     this.userRole = userRole;
    //     this.enabled = enabled;
    //     this.wallet = wallet;
    //     this.idParking = idParking;
    //     this.wallet_enabled = wallet_enabled;
    //     this.numOfCreditCard = numOfCreditCard;
    // }


    public User() {
    }


    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(byte[] imageUrl) {
        this.imageUrl = imageUrl;
    }

    // public String getImageUrl() {
    //     return this.imageUrl;
    // }

    // public void setImageUrl(String imageUrl) {
    //     this.imageUrl = imageUrl;
    // }

    public List<ConfirmationToken> getTokens() {
        return this.tokens;
    }

    public void setTokens(List<ConfirmationToken> tokens) {
        this.tokens = tokens;
    }


    public String getIban() {
        return this.iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return this.userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public Boolean isEnabled() {
        return this.enabled==2;
    }

    public Integer getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Float getWallet() {
        return this.wallet;
    }

    public void setWallet(Float wallet) {
        this.wallet = wallet;
    }

    public Long getIdParking() {
        return this.idParking;
    }

    public void setIdParking(Long idParking) {
        this.idParking = idParking;
    }

    public Boolean isWallet_enabled() {
        return this.wallet_enabled;
    }

    public Boolean getWallet_enabled() {
        return this.wallet_enabled;
    }

    public void setWallet_enabled(Boolean wallet_enabled) {
        this.wallet_enabled = wallet_enabled;
    }

    public String getNumOfCreditCard() {
        return numOfCreditCard;
    }

    public void setNumOfCreditCard(String numOfCreditCard) {
        this.numOfCreditCard = numOfCreditCard;
    }

    public User present(){
        User userForPresent = this;
        userForPresent.setPassword(null);
        return userForPresent;
    }

}
