package parkshare.rest;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import parkshare.domain.User;
import parkshare.service.impl.UserService;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "https://parkshare-front.netlify.app/")
public class AdminController {

    @Autowired
    private UserService userService;

    @GetMapping("")
    @Secured("ROLE_ADMIN")
    public List<User> listUsers(){
        return userService.presentAll();
    }

    @PutMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public User updateUser(@PathVariable("id") long id,@RequestBody User user){
        if(!user.getId().equals(id))
            throw new IllegalArgumentException("User ID must be preserved");
        userService.updateUser(user);
        return userService.presentByUsername(user.getUsername());
    }

    @DeleteMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public User deleteUser(@PathVariable("id") Long id){
        return userService.deleteUser(id);
    }

    @GetMapping("/unconfirmedOwner")
    @Secured("ROLE_ADMIN")
    public List<User> listUnconfirmedOwners(){
        return userService.listUnconfirmedOwners();
    }

    @PostMapping("/confirmOwner/{username}")
    @Secured("ROLE_ADMIN")
    public User confirmOwner(@PathVariable("username") String username){
        return userService.enableUnconfirmedOwner(username);
    }
}
