package parkshare.rest;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import parkshare.domain.UserRole;
import parkshare.service.impl.UserService;

import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

@Service
public class AppUserDetailsService implements UserDetailsService{
    @Value("${parkshare.admin.password}")
    private String adminPasswordHash;

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        parkshare.domain.User appUser=userService.findByUsername(username).orElseThrow(()->
          new UsernameNotFoundException("User with this username not found"));
        if(appUser.getUserRole().equals(UserRole.ADMIN)){
            return new User(username,adminPasswordHash,authorities(username));
        }else
            return new User(username,appUser.getPassword(),authorities(username));
    
    }

    private List<GrantedAuthority> authorities(String username) {
        parkshare.domain.User appUser=userService.findByUsername(username)
                    .orElseThrow( ()-> new UsernameNotFoundException("No user '" + username + "'"));
        if (appUser.getUserRole().equals(UserRole.ADMIN)){
            return commaSeparatedStringToAuthorityList("ROLE_ADMIN");
        }if (appUser.getUserRole().equals(UserRole.CLIENT)){
            return commaSeparatedStringToAuthorityList("ROLE_CLIENT,ROLE_USER");
        }else {
            return commaSeparatedStringToAuthorityList("ROLE_OWNER,ROLE_USER");
        }

    }

    

    
}
