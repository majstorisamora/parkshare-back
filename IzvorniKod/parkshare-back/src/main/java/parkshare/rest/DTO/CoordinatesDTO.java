package parkshare.rest.DTO;

public class CoordinatesDTO {
    private Double x;
    private Double y;

    public CoordinatesDTO(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    @Override
    public String toString() {
        return y + "," + x;
    }
}
