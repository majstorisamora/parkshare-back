package parkshare.rest.DTO;

public class DateTimeDTO {

    private String timeFrom;
    private String timeUntil;
    private Long id;

    public DateTimeDTO(String timeFrom, String timeUntil) {
        this.timeFrom = timeFrom;
        this.timeUntil = timeUntil;
    }

    public DateTimeDTO() {
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeUntil() {
        return timeUntil;
    }

    public void setTimeUntil(String timeUntil) {
        this.timeUntil = timeUntil;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public DateTimeDTO(String timeFrom, String timeUntil, Long id) {
        this.timeFrom = timeFrom;
        this.timeUntil = timeUntil;
        this.id = id;
    }

}
