package parkshare.rest.DTO;

import org.springframework.util.Assert;

public class NewPasswordDTO {
    private String email;


    public NewPasswordDTO() {
    }

    public NewPasswordDTO(String email) {
        Assert.notNull(email, "Email must be set, not null");
        this.email = email;
    }
    public String getEmail() {
        return email;
    }
}

