package parkshare.rest.DTO;

import java.util.Optional;

import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import parkshare.domain.User;

public class ParkingDTO {

    private String name;

    private String description;

    private Float price;

    private String type;

    private Optional<MultipartFile> image;

    private Integer number_of_spots;

    private User owner;

    private String location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Optional<MultipartFile> getImage() {
        return image;
    }

    public void setImage(Optional<MultipartFile> image) {
        this.image = image;
    }

    public Integer getNumberOfSpots() {
        return number_of_spots;
    }

    public void setNumberOfSpots(Integer numberOfSpots) {
        this.number_of_spots = numberOfSpots;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getLocation() { 
        return location; }

    public void setLocation(String location) {
        this.location = location;
    }
    public ParkingDTO() {
    }


    public ParkingDTO(String name, String description, Float price,
                      String type, Optional<MultipartFile> image, Integer numberOfSpots, String location) {
        Assert.notNull(name, "Parking name must be set, not null");
        Assert.notNull(price, "Parking price must be set, not null");
        Assert.notNull(type, "Parking type must be set, not null");
        Assert.notNull(numberOfSpots, "Parking number of spots must be set, not null");
        Assert.notNull(location, "Parking location must be set, not null");

        this.name = name;
        this.description = description;
        this.price = price;
        this.type = type;
        this.image = image;
        this.number_of_spots = numberOfSpots;
        this.location = location;
    }

}
