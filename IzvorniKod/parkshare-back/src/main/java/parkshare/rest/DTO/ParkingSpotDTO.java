package parkshare.rest.DTO;

public class ParkingSpotDTO {

    private Boolean reservation;

    private Boolean taken;

    private String location1;

    private String location2;

    private String location3;

    private String location4;

    public Boolean isReservation() {
        return this.reservation;
    }

    public Boolean getReservation() {
        return this.reservation;
    }

    public void setReservation(Boolean reservation) {
        this.reservation = reservation;
    }

    public Boolean isTaken() {
        return this.taken;
    }

    public Boolean getTaken() {
        return this.taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }

    public String getLocation1() {
        return this.location1;
    }

    public void setLocation1(String location1) {
        this.location1 = location1;
    }

    public String getLocation2() {
        return this.location2;
    }

    public void setLocation2(String location2) {
        this.location2 = location2;
    }

    public String getLocation3() {
        return this.location3;
    }

    public void setLocation3(String location3) {
        this.location3 = location3;
    }

    public String getLocation4() {
        return this.location4;
    }

    public void setLocation4(String location4) {
        this.location4 = location4;
    }

    public ParkingSpotDTO() {
    }

    public ParkingSpotDTO(Boolean reservation, Boolean taken, String location1, String location2, String location3, String location4) {
        this.reservation = reservation;
        this.taken = taken;
        this.location1 = location1;
        this.location2 = location2;
        this.location3 = location3;
        this.location4 = location4;
    }


}
