package parkshare.rest.DTO;

import parkshare.domain.User;

public class ParkingUpdateDTO {
    
    private String name;

    private String description;

    private Float price;

    private String type;


    private Integer numberOfSpots;

    private User owner;

    private String location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getNumberOfSpots() {
        return numberOfSpots;
    }

    public void setNumberOfSpots(Integer numberOfSpots) {
        this.numberOfSpots = numberOfSpots;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getLocation() { 
        return location; }

    public void setLocation(String location) {
        this.location = location;
    }
    public ParkingUpdateDTO() {
    }

    public ParkingUpdateDTO(String name, String description, Float price, String type, Integer numberOfSpots, User owner, String location) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.type = type;
        this.numberOfSpots = numberOfSpots;
        this.owner = owner;
        this.location = location;
    }
}
