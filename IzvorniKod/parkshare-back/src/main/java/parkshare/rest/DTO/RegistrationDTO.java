package parkshare.rest.DTO;

import parkshare.domain.UserRole;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

public class RegistrationDTO {

    private String firstName;
  
    private String lastName;
    
    private String username;
    
    private String email;

    // private byte[] imageUrl;
    // private String imageUrl;
 
    private String iban;
  
    private String password;

    private UserRole userRole;

    private Optional<MultipartFile> file;
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIban() {
        return this.iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return this.userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }



    public RegistrationDTO(String firstName, String lastName, String username, String email, String iban, String password, UserRole userRole, Optional<MultipartFile> file) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.iban = iban;
        this.password = password;
        this.userRole = userRole;
        this.file = file;
    }


    public Optional<MultipartFile> getFile() {
        return this.file;
    }

    public void setFile(Optional<MultipartFile> file) {
        this.file = file;
    }
    public RegistrationDTO() {
    }

    @Override
    public String toString() {
        return "{" +
            " firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", username='" + getUsername() + "'" +
            ", email='" + getEmail() + "'" +
            ", iban='" + getIban() + "'" +
            ", password='" + getPassword() + "'" +
            ", userRole='" + getUserRole() + "'" +
            ", file='" + getFile() + "'" +
            "}";
    }


}
