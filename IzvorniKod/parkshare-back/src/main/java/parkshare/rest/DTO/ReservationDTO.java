package parkshare.rest.DTO;

public class ReservationDTO {

    private Boolean repetitive;

    private String reservation_start;

    private String reservation_end;

    private Float price;


    public Boolean isRepetitive() {
        return this.repetitive;
    }

    public Boolean getRepetitive() {
        return this.repetitive;
    }

    public void setRepetitive(Boolean repetitive) {
        this.repetitive = repetitive;
    }

    public String getReservation_start() {
        return this.reservation_start;
    }

    public void setReservation_start(String reservation_start) {
        this.reservation_start = reservation_start;
    }

    public String getReservation_end() {
        return this.reservation_end;
    }

    public void setReservation_end(String reservation_end) {
        this.reservation_end = reservation_end;
    }

    public Float getPrice() {
        return this.price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }


    public ReservationDTO() {
    }

    public ReservationDTO(Boolean repetitive, String reservation_start, String reservation_end,Float price) {
        this.repetitive = repetitive;
        this.reservation_start = reservation_start;
        this.reservation_end = reservation_end;
        this.price=price;
    }
    
}
