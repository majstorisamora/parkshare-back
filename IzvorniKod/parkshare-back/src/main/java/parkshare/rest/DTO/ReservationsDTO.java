package parkshare.rest.DTO;

import java.util.List;


public class ReservationsDTO {

    private List<Long> ids;
    private String date;

    public List<Long> getIds() {
        return this.ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ReservationsDTO(){
    }

    public ReservationsDTO(List<Long> ids, String date) {
        this.ids = ids;
        this.date = date;
    }

}

