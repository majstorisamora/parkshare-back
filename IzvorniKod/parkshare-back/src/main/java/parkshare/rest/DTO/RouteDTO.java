package parkshare.rest.DTO;

import java.util.List;

public class RouteDTO {
    private List<CoordinatesDTO> coordinates;

    public RouteDTO(List<CoordinatesDTO> coordinates) {
        this.coordinates = coordinates;
    }

    public List<CoordinatesDTO> getCoordinates() {
        return coordinates;
    }
}
