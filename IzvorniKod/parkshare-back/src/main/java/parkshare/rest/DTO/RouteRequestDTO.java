package parkshare.rest.DTO;

public class RouteRequestDTO {
    private CoordinatesDTO start;
    private String type;
    private Long duration;
    //private String endOfReservation;

    // public RouteRequestDTO(CoordinatesDTO coordinates, String type, String endOfReservation) {
    //     this.start = coordinates;
    //     this.type = type;
    //     this.endOfReservation = endOfReservation;
    // }

    public CoordinatesDTO getStart() {
        return start;
    }

    public String getType() {
        return type;
    }

    // public String getEndOfReservation() {
    //     return endOfReservation;
    // }
    public void setStart(CoordinatesDTO start) {
        this.start = start;
    }
    public void setType(String type) {
        this.type = type;
    }

    public Long getDuration() {
        return this.duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }
    // public void setEndOfReservation(String endOfReservation) {
    //     this.endOfReservation = endOfReservation;
    // }

    public RouteRequestDTO() {
    }

    public RouteRequestDTO(CoordinatesDTO start, String type, Long duration) {
        this.start = start;
        this.type = type;
        this.duration = duration;
    }

}
