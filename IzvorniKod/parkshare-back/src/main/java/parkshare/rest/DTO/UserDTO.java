package parkshare.rest.DTO;

public class UserDTO {


    private String firstName;
  
    private String lastName;
    
    private String username;
    
    private String email;

    private String imageUrl;
 
    private String iban;
  
    private Boolean wallet_enabled;

    private String numOfCreditCard;


    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getIban() {
        return this.iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public Boolean isWallet_enabled() {
        return this.wallet_enabled;
    }

    public Boolean getWallet_enabled() {
        return this.wallet_enabled;
    }

    public void setWallet_enabled(Boolean wallet_enabled) {
        this.wallet_enabled = wallet_enabled;
    }

    public String getNumOfCreditCard() {
        return this.numOfCreditCard;
    }

    public void setNumOfCreditCard(String numOfCreditCard) {
        this.numOfCreditCard = numOfCreditCard;
    }


    public UserDTO() {
    }


    public UserDTO(String firstName, String lastName, String username, String email, String imageUrl, String iban, Boolean wallet_enabled, String numOfCreditCard) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.imageUrl = imageUrl;
        this.iban = iban;
        this.wallet_enabled = wallet_enabled;
        this.numOfCreditCard = numOfCreditCard;
    }
    

}
