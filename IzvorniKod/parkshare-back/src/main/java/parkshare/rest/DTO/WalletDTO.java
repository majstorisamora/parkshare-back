package parkshare.rest.DTO;

import org.springframework.util.Assert;



public class WalletDTO {
    private String numOfCreditCard;
    private String expirationDate;
    private String cvc;
    private Float amount;

    public WalletDTO() {
    }

    public WalletDTO(String numOfCreditCard, Float amount) {
        Assert.notNull(numOfCreditCard, "Number of credit cart must be set, not null");
        Assert.notNull(amount, "Amount must be set, not null");

        this.numOfCreditCard = numOfCreditCard;
        this.amount = amount;
    }

    public WalletDTO(String numOfCreditCard, String expirationDate, String cvc) {
        Assert.notNull(numOfCreditCard, "Number of credit cart must be set, not null");
        Assert.notNull(cvc, "CVC must be set, not null");
        Assert.notNull(expirationDate, "Expiration date of credit card must be set, not null");

        this.numOfCreditCard = numOfCreditCard;
        this.expirationDate = expirationDate;
        this.cvc = cvc;
    }

    public String getNumOfCreditCard() {
        return numOfCreditCard;
    }

    public void setNumOfCreditCard(String numOfCreditCard) {
        this.numOfCreditCard = numOfCreditCard;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }
}
