package parkshare.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import parkshare.domain.User;

import parkshare.rest.DTO.LoginDTO;
import parkshare.rest.DTO.NewPasswordDTO;
import parkshare.service.JwtUtil;
import parkshare.service.NewPasswordService;
import parkshare.service.impl.UserService;

import java.util.Optional;

@RestController
@RequestMapping("")
@CrossOrigin(origins = "https://parkshare-front.netlify.app/")
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private AppUserDetailsService appUserDetailsService;

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private NewPasswordService newPasswordService;



    @PostMapping("/login")
    public Optional<User> loginUser(@RequestBody LoginDTO dto) {
       return this.userService.findUser(dto.getUsername(), dto.getPassword());
    }

    @PostMapping("/authenticate")
    //@PostMapping("/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginDTO LoginDTO) {
        //if(userService.findUser(LoginDTO.getUsername(),LoginDTO.getPassword())!=null){
            Authentication auth= authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(LoginDTO.getUsername(),LoginDTO.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(auth);
        UserDetails userDetails =appUserDetailsService.loadUserByUsername(LoginDTO.getUsername());
        String jwt=jwtUtil.generateToken(userDetails);
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
       // }
       // return null;
    }

    @PostMapping("/forgotPassword")
    public void forgotPassword(@RequestBody NewPasswordDTO passdto) {
        this.newPasswordService.changePassword(passdto.getEmail());
    }

   
}