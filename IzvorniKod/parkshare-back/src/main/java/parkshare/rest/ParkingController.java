package parkshare.rest;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import parkshare.domain.Parking;
import parkshare.exception.RequestDeniedException;
import parkshare.rest.DTO.ParkingDTO;
import parkshare.rest.DTO.ParkingUpdateDTO;
import parkshare.service.JwtUtil;
import parkshare.service.ParkingService;
import parkshare.service.impl.UserService;


@RestController
@RequestMapping("/parking")
@CrossOrigin(origins = "https://parkshare-front.netlify.app/")
public class ParkingController {

    @Autowired
    private ParkingService parkingService;
    @Autowired
    private UserService userService;
    @Autowired
    private JwtUtil jwtUtil;

    @GetMapping
    @Secured("ROLE_OWNER")
    public Parking getCurrentParking(@RequestHeader("Authorization") String auth) {
        String token=auth.substring(7,auth.length());
        String username=jwtUtil.extractUsername(token);
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));
        return parkingService.findById(currentUser.getIdParking()).get();
    }

    @GetMapping("/alltype")
    @Secured("ROLE_CLIENT")
    public List<Parking> getParkings(@RequestHeader("Authorization") String auth, @RequestBody String type) {
        String token = auth.substring(7, auth.length());
        String username = jwtUtil.extractUsername(token);
        //Provjera je li korisnik registriran u sustavu
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));
        return this.parkingService.getAllParkings(type);
    }

    @PostMapping("")
    @Secured("ROLE_OWNER")
    public Parking addParking(@RequestHeader("Authorization") String auth, @ModelAttribute ParkingDTO request) throws IOException {
        //provjera je li korisnik koji salje zahtjev registriran u sustav
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));
                System.out.println(request.getNumberOfSpots()+"ifudshi");
        return parkingService.addParking(currentUser, request);
    }

    @PutMapping("")
    @Secured("ROLE_OWNER")
    public Parking updateParking(@RequestHeader("Authorization") String auth, @ModelAttribute ParkingUpdateDTO request) {
        //provjera je li korisnik koji salje zahtjev registriran u sustav
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));

        if(!parkingService.findById(currentUser.getIdParking()).isPresent())
            throw new RequestDeniedException("Parking does not exist");
        return parkingService.updateParking(currentUser, request);
    }

    @GetMapping("/all")
    public List<Parking> findAll(){
        return parkingService.findAll();
    }
}
