package parkshare.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import parkshare.domain.ParkingSpot;
import parkshare.exception.RequestDeniedException;
import parkshare.rest.DTO.*;
import parkshare.service.JwtUtil;
import parkshare.service.ParkingSpotsService;
import parkshare.service.ReservationService;
import parkshare.service.impl.UserService;

import java.util.*;

@RestController
@RequestMapping("/parkingspots")
@CrossOrigin(origins = "https://parkshare-front.netlify.app/")
public class ParkingSpotController {

    @Autowired
    private ParkingSpotsService parkingSpotsService;
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private UserService userService;

    @PostMapping("/{id}")
    @Secured({"ROLE_OWNER", "ROLE_ADMIN"})
    public ParkingSpot drawParkingSpot(@PathVariable("id") Long id,@RequestBody ParkingSpotDTO parkingSpotDTO){
        return parkingSpotsService.draw(parkingSpotDTO,id);        
    }
    @GetMapping("/{id}")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public List<ParkingSpot> listUsers(@PathVariable("id") long id){
        return parkingSpotsService.listAll(id);
    }

    @PostMapping("")
    @Secured("ROLE_CLIENT")
    public List<List<ParkingSpot>> setDateTime(@RequestHeader("Authorization") String auth,
                                         @RequestBody DateTimeDTO req) {
        //provjera je li korisnik koji salje zahtjev registriran u sustav
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));

        return parkingSpotsService.setDateTime(currentUser, req);
    }

    @PostMapping("/generateRouteToNearest")
    @Secured("ROLE_USER")
    public RouteDTO generateRouteToNearest(@RequestHeader("Authorization") String auth,@RequestBody RouteRequestDTO request){
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User user = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));

        return parkingSpotsService.generateRouteToNearest(request,user);
    }

    
}
