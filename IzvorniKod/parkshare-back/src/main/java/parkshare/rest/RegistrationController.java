package parkshare.rest;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import parkshare.rest.DTO.RegistrationDTO;
import parkshare.service.RegistrationService;

@RestController
@RequestMapping("/register")
@CrossOrigin(origins = "https://parkshare-front.netlify.app/")
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @PostMapping("")
    public String register(@ModelAttribute RegistrationDTO request) throws IOException{
        //@RequestParam("request") String request,@RequestParam("file") MultipartFile file) throws IOException{
        return registrationService.register(request);
    }
    @GetMapping("confirm")
    public String confirm(@RequestParam("token") String token){
        return registrationService.confirmToken(token);
    }

    /*lista promjena
    user, aplication.properties
    registrationDTO, userrepository
     */

}
