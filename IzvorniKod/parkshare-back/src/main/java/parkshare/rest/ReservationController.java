package parkshare.rest;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import parkshare.domain.Reservation;
import parkshare.exception.RequestDeniedException;
import parkshare.rest.DTO.ReservationDTO;
import parkshare.rest.DTO.ReservationsDTO;
import parkshare.service.JwtUtil;
import parkshare.service.ReservationService;
import parkshare.service.impl.UserService;

@RestController
@RequestMapping("/reservation")
@CrossOrigin(origins = "https://parkshare-front.netlify.app/")
public class ReservationController{

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/{id}")
    @Secured("ROLE_CLIENT")
    public parkshare.domain.User makeReservation(@PathVariable("id") Long id,@RequestHeader("Authorization") String auth,@RequestBody ReservationDTO reservationDTO){
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));
        return reservationService.makeReservation(currentUser,reservationDTO,id);
    }
    @PostMapping()
    @Secured("ROLE_CLIENT")
    public List<Reservation> makeReservations(@RequestHeader("Authorization") String auth,@RequestBody ReservationsDTO reservationsDTO) {
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));
        return reservationService.filter(reservationsDTO);
    }
  
    @GetMapping("/user")
    @Secured("ROLE_CLIENT")
    public List<Reservation> allReservations(@RequestHeader("Authorization") String auth){
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));
        return reservationService.allReservations(currentUser);

    }
    
}
