package parkshare.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import parkshare.exception.RequestDeniedException;
import parkshare.service.JwtUtil;
import parkshare.service.StatisticsService;
import parkshare.service.impl.UserService;

import java.util.List;

@RestController
@RequestMapping("/statistics")
@CrossOrigin(origins = "https://parkshare-front.netlify.app/")
public class StatisticsController {

    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private UserService userService;
    @Autowired
    private StatisticsService statisticsService;

    @GetMapping("")
    @Secured("ROLE_OWNER")
    public List<Integer> getNoOfResPerMonth(@RequestHeader("Authorization") String auth) {
        String token = auth.substring(7, auth.length());
        String username = jwtUtil.extractUsername(token);
        //Provjera je li korisnik registriran u sustavu
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));

        return statisticsService.getNoOfResPerMonth(currentUser);
    }

}
