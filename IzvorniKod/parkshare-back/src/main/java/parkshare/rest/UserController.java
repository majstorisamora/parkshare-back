package parkshare.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import parkshare.domain.ParkingSpot;
import parkshare.exception.RequestDeniedException;
import parkshare.rest.DTO.UserDTO;
import parkshare.service.JwtUtil;
import parkshare.service.ParkingSpotsService;
import parkshare.service.impl.UserService;

/*
@Profile("form-security")*/
@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "https://parkshare-front.netlify.app/")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ParkingSpotsService parkingSpotService;

    @Autowired
    JwtUtil jwtUtil;

    @GetMapping
    public parkshare.domain.User getCurrentUser(@RequestHeader("Authorization") String auth) {
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        return userService.presentByUsername(username);
    }

    @PutMapping
    public parkshare.domain.User updateUser(@RequestHeader("Authorization") String auth, @RequestBody UserDTO userDTO) {
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User oldUser = userService.findByUsername(username).orElseThrow(() -> new RequestDeniedException("Unexpected exception"));
        Long id=oldUser.getId();
        return userService.updateUserData(id, userDTO);
    }

    @PostMapping("/takeParking/{id}")
    @Secured("ROLE_CLIENT")
    public ParkingSpot takeParking(@RequestHeader("Authorization") String auth,@PathVariable("id") Long id) {
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User user = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));
        ParkingSpot parkingSpot = parkingSpotService.findById(id).orElseThrow(() -> new RequestDeniedException("Parking Spot id: " + id + " does not exist"));
        return this.parkingSpotService.takeParking(user,parkingSpot);
    }


}