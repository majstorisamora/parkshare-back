package parkshare.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import parkshare.domain.User;
import parkshare.exception.RequestDeniedException;
import parkshare.rest.DTO.WalletDTO;
import parkshare.service.JwtUtil;
import parkshare.service.impl.UserService;

@RestController
@RequestMapping("/user/wallet")
@CrossOrigin(origins = "https://parkshare-front.netlify.app/")
public class WalletController {

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    private UserService userService;

    @GetMapping("")
    @Secured("ROLE_CLIENT")
    public ResponseEntity<?> checkWalletStatus(@RequestHeader("Authorization") String auth) {
        //provjera je li korisnik koji salje zahtjev registriran u sustav
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));

        return ResponseEntity.ok(this.userService.checkWalletStatus(currentUser));
    }

    @GetMapping("/activate")
    @Secured("ROLE_CLIENT")
    public User activateWallet(@RequestHeader("Authorization") String auth) {
        //provjera je li korisnik koji salje zahtjev registriran u sustav
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));

        return this.userService.activateWallet(currentUser);
    }

    /*
    @PutMapping("")
    @Secured("ROLE_CLIENT")
    public void updateWallet(@RequestHeader("Authorization") String auth, @RequestBody WalletDTO dto) {
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));
        this.userService.updateWallet(currentUser, dto);
    }
    */

    @PutMapping("/deposit")
    @Secured("ROLE_CLIENT")
    public User depositInWallet(@RequestHeader("Authorization") String auth, @RequestBody WalletDTO dto) {
        String username = jwtUtil.extractUsername(auth.substring(7, auth.length()));
        parkshare.domain.User currentUser = this.userService.findByUsername(username)
                .orElseThrow(() -> new RequestDeniedException("User with username: " + username + " does not exist"));
        return this.userService.depositInWallet(currentUser, dto);
    }
}
