package parkshare.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import parkshare.dao.ParkingRepository;
import parkshare.dao.UserRepository;
import parkshare.domain.Parking;
import parkshare.domain.User;
import parkshare.domain.UserRole;
import parkshare.exception.RequestDeniedException;
import parkshare.rest.DTO.ParkingDTO;
import parkshare.rest.DTO.ParkingUpdateDTO;
import parkshare.service.Email.EmailSender;

import javax.transaction.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class ParkingService {

    @Autowired
    private ParkingRepository parkingRepository;
    @Autowired
    private EmailSender emailSender;
    @Autowired
    private UserRepository userRepository;

    public Optional<Parking> findByName(String name) {
        return parkingRepository.findByName(name);
    }
    public Optional<Parking> findById(Long id) {return parkingRepository.findById(id);}

    @Transactional
    public Parking addParking(User owner, ParkingDTO request) throws IOException {
        //provjera je li user vec unio jedan parking u sustav
        if(owner.getIdParking() != null) throw new RequestDeniedException("User has already added a parking");
        //provjera preduvjeta->registriran+vlasnikparkinga
        if(owner.getEnabled() != 2) throw new RequestDeniedException("User must be registered");
        if(owner.getUserRole() != UserRole.PARKINGOWNER) throw new RequestDeniedException("User must be a parking owner");
        //provjera je li name unique
        if(parkingRepository.findByName(request.getName()).isPresent())
            throw new RequestDeniedException("This parking name is already taken. Please choose another name.");
        //provjera je li lokacija oblika x;y
        String[] location = request.getLocation().split(";");
        if(location == null || location.length != 2 ||Float.isNaN(Float.parseFloat(location[0]))
                || Float.isNaN(Float.parseFloat(location[1])))
            throw new RequestDeniedException("Location coordinates must be in the form of 'x;y' where x and y are float values");

        //provjeriti jeli tip parkinga tocan
        if(!request.getType().equals("bike") && !request.getType().equals("car")){
            throw new RequestDeniedException("Type of parking must be car or bike");
        }
        if(request.getType().equals("car")) request.setNumberOfSpots(0);
        Integer num=request.getNumberOfSpots();
        System.out.println(num+ "ofsdj");
        Parking parking = new Parking(request.getName(), request.getDescription(), request.getPrice(),
                request.getType(),request.getImage().get().getBytes(), num, owner, request.getLocation());
        parkingRepository.save(parking);
        userRepository.updateUserParking(owner.getUsername(), parking.getId_parking());
        emailSender.send(owner.getEmail(), buildEmail(owner.getUsername()));
        return parkingRepository.save(parking);
    }

    public Parking updateParking(User owner, ParkingUpdateDTO request) {
        if(owner.getEnabled() != 2) throw new RequestDeniedException("User must be registered");
        if(owner.getUserRole() != UserRole.PARKINGOWNER) throw new RequestDeniedException("User must be a parking owner");

        Parking parking = findById(owner.getIdParking()).get();

        if(owner.getIdParking() == null || parking == null) throw new RequestDeniedException("User must have a parking added");

        //provjera za unique name
        if(!parking.getName().equals(request.getName())) {
            if(parkingRepository.findByName(request.getName()).isPresent())
                throw new RequestDeniedException("Parking name must be unique");
        }

        String[] location = request.getLocation().split(";");
        if(location == null || location.length != 2 ||Float.isNaN(Float.parseFloat(location[0]))
                || Float.isNaN(Float.parseFloat(location[1])))
            throw new RequestDeniedException("Location coordinates must be in the form of 'x;y' where x and y are float values");
            
        parking.setLocation(request.getLocation());
        parking.setName(request.getName());
        parking.setPrice(request.getPrice());
        parking.setType(request.getType());
        parking.setDescription(request.getDescription());

        return parkingRepository.save(parking);
    }

    public List<Parking> getAllParkings(String type) {
        if (!type.isEmpty()) {
            if (type.equals("bike") || type.equals("car")) {
                return this.parkingRepository.findByType(type);
            } else {
                throw new RequestDeniedException("Entered type of vehicle: " + type + "  is not valid");
            }
        } else {
            throw new RequestDeniedException("Type of vehicle must be set, not null");        }
    }

    public List<Parking> findAll() {
        List<Parking> par=parkingRepository.findAll();
        for(Parking p :par){
            p.setOwner(null);
        }
        return par;
    }

    private String buildEmail(String name) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirmation email</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> You have successfully added your parking! Thank you for using our app. </p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }

}
