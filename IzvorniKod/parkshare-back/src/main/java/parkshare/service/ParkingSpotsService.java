package parkshare.service;

import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import parkshare.dao.ParkingRepository;
import parkshare.dao.ParkingSpotsRepository;
import parkshare.dao.ReservationRepository;
import parkshare.domain.*;
import parkshare.exception.RequestDeniedException;
import parkshare.rest.DTO.*;
import parkshare.service.impl.UserService;

@Service
public class ParkingSpotsService {

    @Autowired
    private ParkingSpotsRepository parkingSpotRepository;

    @Autowired
    private ParkingRepository parkingRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private ReservationService reservationService;

    public ParkingSpot draw(ParkingSpotDTO parkingSpotDTO,Long id) {
        String[] location = parkingSpotDTO.getLocation1().split(";");
        if(location == null || location.length != 2 ||Float.isNaN(Float.parseFloat(location[0]))
                || Float.isNaN(Float.parseFloat(location[1])))
            throw new RequestDeniedException("Location coordinates must be in the form of 'x;y' where x and y are float values");
        location = parkingSpotDTO.getLocation2().split(";");
            if(location == null || location.length != 2 ||Float.isNaN(Float.parseFloat(location[0]))
                    || Float.isNaN(Float.parseFloat(location[1])))
                throw new RequestDeniedException("Location coordinates must be in the form of 'x;y' where x and y are float values");
        location = parkingSpotDTO.getLocation3().split(";");
            if(location == null || location.length != 2 ||Float.isNaN(Float.parseFloat(location[0]))
                    || Float.isNaN(Float.parseFloat(location[1])))
                throw new RequestDeniedException("Location coordinates must be in the form of 'x;y' where x and y are float values");
        location = parkingSpotDTO.getLocation4().split(";");
            if(location == null || location.length != 2 ||Float.isNaN(Float.parseFloat(location[0]))
                    || Float.isNaN(Float.parseFloat(location[1])))
                throw new RequestDeniedException("Location coordinates must be in the form of 'x;y' where x and y are float values");

            Optional<Parking> parkingO = parkingRepository.findById(id);
            Parking parking=parkingO.get();
            parking.setNumber_of_spots(parking.getNumber_of_spots()+1);
            ParkingSpot parkingSpot=new ParkingSpot(parkingSpotDTO.getReservation(),false,
                parkingSpotDTO.getLocation1(),  parkingSpotDTO.getLocation2(),
                parkingSpotDTO.getLocation3(),  parkingSpotDTO.getLocation4(),parking);
         return parkingSpotRepository.save(parkingSpot);
    }

    public List<ParkingSpot> listAll(long id) {
        return parkingSpotRepository.findAll().stream().filter((spot) -> spot.getParking().getId_parking() == id).toList();
    }

    public Optional<ParkingSpot> findById(long id){return parkingSpotRepository.findById(id);}

    public ParkingSpot takeParking(User user, ParkingSpot parkingSpot) {
        if(user.getIdParking() != null){
            throw new RequestDeniedException("User already is on parking");
        }
        if(parkingSpot.isTaken()){
            throw new RequestDeniedException("Parking spot is already taken");
        }
        userService.payFromWallet(user,parkingSpot.getParking().getPrice());
        parkingSpot.setTaken(true);
        return parkingSpotRepository.save(parkingSpot);
    }

    public ParkingSpot freeParking(ParkingSpot parkingSpot) {
        parkingSpot.setTaken(false);
        return parkingSpotRepository.save(parkingSpot);
    }

    private Boolean overlap(LocalDateTime newStart,LocalDateTime newEnd,
                             LocalDateTime oldStart,LocalDateTime oldEnd){
        System.out.println(newStart.compareTo(oldStart)>=0);
        System.out.println(newStart.compareTo(oldEnd)<=0);
        if(((newStart.compareTo(oldStart)>=0 && newStart.compareTo(oldEnd)<=0)||
          (newEnd.compareTo(oldStart)>=0 && newEnd.compareTo(oldEnd)<=0)) ||
          (newStart.compareTo(oldStart)<=0 && newEnd.compareTo(oldStart)>=0)) {
              System.out.println("Overlapaju se");
              return false;
        }
         return true;
     }

     private Boolean filterReservation(LocalDateTime start,LocalDateTime end,Long id){
         boolean can=true;
         List<Reservation> reservations=reservationRepository.findAll();
         for(Reservation reservation:reservations){
             if(id.equals(reservation.getParkingSpot().getId_parkingSpot())){
                 if(reservation.getRepetitive()){
                     DayOfWeek dayOfWeek=reservation.getReservation_start().getDayOfWeek();
                     DayOfWeek nOfWeek=reservation.getReservation_end().getDayOfWeek();
                     if(dayOfWeek.equals(start.getDayOfWeek()) || dayOfWeek.equals(end.getDayOfWeek())
                      || nOfWeek.equals(start.getDayOfWeek()) || nOfWeek.equals(end.getDayOfWeek())){
                        if (!overlap(start,end,reservation.getReservation_start(),reservation.getReservation_end())){
                            can=false;
                            throw new RequestDeniedException("Parking spot is already taken at that time.");
                        }
                } 
            }else {
                if (!overlap(start,end,reservation.getReservation_start(),reservation.getReservation_end())){
                    can=false;
                    throw new RequestDeniedException("Parking spot is already taken at that time.");
                }
                 }
             }
         }
         return can;
     }
     private Boolean overlapTime(LocalTime newStart,LocalTime newEnd,
                                    LocalTime oldStart,LocalTime oldEnd){
            if(((newStart.compareTo(oldStart)>=0 && newStart.compareTo(oldEnd)<=0)||
                    (newEnd.compareTo(oldStart)>=0 && newEnd.compareTo(oldEnd)<=0)) ||
                    (newStart.compareTo(oldStart)<=0 && newEnd.compareTo(oldStart)>=0)) {
                 System.out.println("Overlapaju se");
            return false;
        }
            return true;
        }

    //vraca list<lista parkingspots koji se mogu rezervirati,lista parkingspots koji imaju rezervaciju za to vrijeme>
    public List<List<ParkingSpot>> setDateTime(User client, DateTimeDTO request) {
        //parsiranje stringova timeFrom i timeUntil u LocalDateTime
        LocalDateTime dateTimeFrom = LocalDateTime.parse(request.getTimeFrom());
        LocalDateTime dateTimeUntil = LocalDateTime.parse(request.getTimeUntil());

        //preduvjet: korisnik registriran i ima prava klijenta
        if(client.getEnabled() != 2) throw new RequestDeniedException("User must be registered and confirmed by admin");
        if(client.getUserRole() != UserRole.CLIENT) throw new RequestDeniedException("User must be a client");

        if(parkingRepository.findById(request.getId())==null){
            throw new RequestDeniedException("Wrong parking id");
        }
        List<ParkingSpot> allParkingSpots = listAll(request.getId());
        List<ParkingSpot> availableSpots = new ArrayList<>();
        List<ParkingSpot> takenSpots = new ArrayList<>();
        List<List<ParkingSpot>> returnList = new ArrayList<>();
        List<Reservation> allReservations = reservationRepository.findAllReservations();
        
        for(ParkingSpot spot : allParkingSpots){
            if(spot.isReservation()) {
                //provjera je li mjesto rezervirano u to vrijeme
                for (Reservation reservation : allReservations) {
                    if(spot.getId_parkingSpot() == reservation.getParkingSpot().getId_parkingSpot()){
                    if(!overlap(dateTimeFrom,dateTimeUntil,reservation.getReservation_start(),
                            reservation.getReservation_end()) && !reservation.getRepetitive())
                        takenSpots.add(spot);
                    else if(reservation.getRepetitive()) {
                        if(dateTimeFrom.getDayOfWeek() == reservation.getReservation_start().getDayOfWeek() &&
                                dateTimeFrom.getDayOfWeek() == reservation.getReservation_end().getDayOfWeek() &&
                                dateTimeUntil.getDayOfWeek() == reservation.getReservation_start().getDayOfWeek() &&
                                dateTimeUntil.getDayOfWeek() == reservation.getReservation_end().getDayOfWeek()) {
                            LocalTime newStart = dateTimeFrom.toLocalTime();
                            LocalTime newEnd = dateTimeUntil.toLocalTime();
                            LocalTime oldStart = reservation.getReservation_start().toLocalTime();
                            LocalTime oldEnd = reservation.getReservation_end().toLocalTime();
                            //TODO OVERLAP
                           if(!overlapTime(newStart,newEnd,oldStart,oldEnd))
                                takenSpots.add(spot);
                        } else if(dateTimeFrom.getDayOfWeek() == reservation.getReservation_start().getDayOfWeek() &&
                                dateTimeUntil.getDayOfWeek() == reservation.getReservation_end().getDayOfWeek() &&
                                dateTimeFrom.getDayOfWeek() != dateTimeUntil.getDayOfWeek()){
                            //ako je start isti dan i end isti dan ali end je drugi dan
                            takenSpots.add(spot);
                        }
                    }
                }
                }
                //slobodna mjesta koje je moguce rezervirati
                if(!takenSpots.contains(spot))
                  availableSpots.add(spot);
            }
        }

        returnList.add(availableSpots);
        returnList.add(takenSpots);

        return returnList;
    }

    private String getOsrmRoute(CoordinatesDTO start, CoordinatesDTO finish, String type) {
        String url = "http://router.project-osrm.org/route/v1/" + type
                + "/" + start.toString() + ";" + finish.toString()
                + "?geometries=geojson" + "&overview=full";

        RestTemplate restTemplate = new RestTemplate();
        System.out.println(url);
        return restTemplate.getForObject(url, String.class);
    }

    private RouteDTO generateRoute(CoordinatesDTO start, CoordinatesDTO finish, String type){
        String rawJson = getOsrmRoute(start, finish, type);
        List<CoordinatesDTO> listOfCoordinates = new ArrayList<CoordinatesDTO>();
        try {
            JSONObject root = new JSONObject(rawJson);
            //if(root.getString("code") != "Ok"){throw new RequestDeniedException("OSRM API iznimka:"+root.toString());}
            JSONArray jsonCoordinatesArray = root.getJSONArray("routes").
                    getJSONObject(0).getJSONObject("geometry").getJSONArray("coordinates");
            for(int i = 0; i < jsonCoordinatesArray.length();i++){
                JSONArray jsonCoordinates = jsonCoordinatesArray.getJSONArray(i);
                CoordinatesDTO coordinates = new CoordinatesDTO(jsonCoordinates.getDouble(1),jsonCoordinates.getDouble(0));
                listOfCoordinates.add(coordinates);
            }

        } catch (JSONException e) {
            throw new RequestDeniedException("Problem u parsiranju kod generiranja rute");
        }

        return new RouteDTO(listOfCoordinates);
    }

    private Double calculateDistance(CoordinatesDTO start, CoordinatesDTO finish, String type){
        String rawJson = getOsrmRoute(start, finish, type);
        try {
            JSONObject root = new JSONObject(rawJson);
            //if(root.getString("code") != "Ok"){throw new RequestDeniedException("OSRM API iznimka");}
            Double distance = root.getJSONArray("routes").
                    getJSONObject(0).getDouble("distance");
            return distance;
        } catch (JSONException e) {
            throw new RequestDeniedException("Problem u parsiranju kod računanja udaljenosti");
        }
    }

    private CoordinatesDTO format(String string){
        String[] array = string.split(";");
        return new CoordinatesDTO(Double.parseDouble(array[0]),Double.parseDouble(array[1]));
    }

    public RouteDTO generateRouteToNearest(RouteRequestDTO request,User user){
        CoordinatesDTO start = request.getStart();
        String type = request.getType();


        if (type.equals("car")){
            List<ParkingSpot> allParkingSpotsRaw = parkingSpotRepository.findAll().stream().filter(parkingSpot -> !parkingSpot.getTaken()).toList();
            if(allParkingSpotsRaw.size() == 0){throw new RequestDeniedException("Ne postoje parkirna mjesta za automobil");}
            List<ParkingSpot> allParkingSpots = new ArrayList<ParkingSpot>(allParkingSpotsRaw);
            allParkingSpots.sort((parkingSpot, t1) ->
                    Double.compare(calculateDistance(start,format(parkingSpot.getLocation1()),type),calculateDistance(start,format(t1.getLocation1()),type)));
            ParkingSpot nearestParkingSpot = allParkingSpots.get(0);
            CoordinatesDTO finish = format(nearestParkingSpot.getLocation1());
            Float price=nearestParkingSpot.getParking().getPrice();
            ReservationDTO reservationDTO = new ReservationDTO(false,LocalDateTime.now().plusMinutes(30).toString(),
                    LocalDateTime.now().plusHours(request.getDuration()).plusMinutes(30).toString(),price*request.getDuration());

            reservationService.makeReservation(user,reservationDTO,nearestParkingSpot.getId_parkingSpot());
            return generateRoute(start,finish,type);
        } else if (type.equals("bike")){
            List<Parking> allParkingsRaw = parkingRepository.findAll().stream().filter(parking -> parking.getType().equals("bike")).toList();
            if(allParkingsRaw.size() == 0){throw new RequestDeniedException("Ne postoje parkirališta za biciklu");}
            List<Parking> allParkings = new ArrayList<>(allParkingsRaw);
            allParkings.sort((parking, t1) -> Double.compare(calculateDistance(start,format(parking.getLocation()),type),calculateDistance(start,format(parking.getLocation()),type)));
            Parking nearestParking = allParkings.get(0);
            CoordinatesDTO finish = format(nearestParking.getLocation());
            //nearestParking.setNumber_of_spots(nearestParking.getNumber_of_spots() - 1);
            return generateRoute(start,finish,type);
        } else {
            throw new RequestDeniedException("Nepoznat tip");
        }
    }
    
}




















