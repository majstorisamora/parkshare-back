package parkshare.service;

import java.io.IOException;
import java.time.LocalDateTime;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import parkshare.domain.ConfirmationToken;
import parkshare.domain.User;
import parkshare.domain.UserRole;
import parkshare.exception.RequestDeniedException;
import parkshare.rest.DTO.RegistrationDTO;
import parkshare.service.Email.EmailSender;
import parkshare.service.Email.EmailValidator;
import parkshare.service.impl.UserService;


@Service
public class RegistrationService  {
    private static final String IBAN_FORMAT="HR[0-9]{19}";
    @Autowired
    private EmailValidator emailValidator;
  

    @Autowired
    private UserService userService;
    @Autowired
    private EmailSender emailSender;
    @Autowired
    private ConfirmationTokenService confirmationTokenService;

    public String register(RegistrationDTO request) throws IOException  {
        //provjera maila
        boolean isValid=emailValidator.test(request.getEmail());
        if(!isValid){
            throw new RequestDeniedException("Email not valid");
        }
        //provjera ibana
        boolean goodIban=request.getIban().matches(IBAN_FORMAT);
        if(!goodIban){
            throw new RequestDeniedException("IBAN must start with HR and have 19 digits");
        }
        byte[] image=null;
        if(!request.getFile().isEmpty())
            image= request.getFile().get().getBytes();

        User user = new User(request.getFirstName(),request.getLastName(),
                              request.getUsername(),request.getPassword(),request.getEmail(),
                              image,request.getIban(),request.getUserRole(),
                              0,false,null,null, null);
         String token=userService.singUpUser(user);
        String link="http://localhost:8080/register/confirm?token="+token;
        emailSender.send(user.getEmail(), buildEmail(user.getUsername(), link));
        return token;
    }

    @Transactional
    public String confirmToken(String token){
        //provjerava postoji li token
      ConfirmationToken confToken=confirmationTokenService.getToken(token).
                                   orElseThrow(()-> new RequestDeniedException("Token not found"));
        //ako je email vec potvrden bazi iznimku
        if(confToken.getConfirmedAt()!=null){
            throw new RequestDeniedException("Email already confirmed at "+ confToken.getConfrimedAt());
        }
        //provjera jeli token expired
        LocalDateTime expiredAt=confToken.getExpiresAt();
        if(expiredAt.isBefore(LocalDateTime.now())){
            throw new RequestDeniedException("Token expired at "+ expiredAt);
        }
        //postavljanje da je token potvrden
        confirmationTokenService.setConfirmedAt(token);
        //atribut enabled u Useru postaje true ako se ne radi o ParkingOwneru
        //ako se pak radi o ParkingOwneru njegov username se dodaje u tablicu
        //UnconfirmedOwner gdje čeka potvrdu od strane admina
        if(confToken.getUser().getUserRole() == UserRole.PARKINGOWNER){
            userService.askAdminConfirmation(confToken.getUser().getUsername());
        } else {
            userService.enableUser(confToken.getUser().getUsername());
        }
        return "Email confirmed :)";
    }

    private String buildEmail(String name, String link) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Thank you for registering. Please click on the below link to activate your account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Activate Now</a> </p></blockquote>\n Link will expire in 24 hours. <p>See you soon</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }
    
}
