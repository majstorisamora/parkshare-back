package parkshare.service;


import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import parkshare.dao.ParkingSpotsRepository;
import parkshare.dao.ReservationRepository;
import parkshare.domain.User;
import parkshare.exception.RequestDeniedException;
import parkshare.rest.DTO.ReservationDTO;
import parkshare.rest.DTO.ReservationsDTO;
import parkshare.domain.ParkingSpot;
import parkshare.domain.Reservation;
import java.util.ArrayList;
import java.util.List;


@Service
public class ReservationService {

     @Autowired
     private ReservationRepository reservationRepository;
     @Autowired
     private ParkingSpotsRepository parkingSpotRepository;

//false se vrati ako se ne moze rez
     private Boolean overlap(LocalDateTime newStart,LocalDateTime newEnd,
                             LocalDateTime oldStart,LocalDateTime oldEnd){
        if(((newStart.compareTo(oldStart)>=0 && newStart.compareTo(oldEnd)<=0)||
          (newEnd.compareTo(oldStart)>=0 && newEnd.compareTo(oldEnd)<=0)) ||
          (newStart.compareTo(oldStart)<=0 && newEnd.compareTo(oldStart)>=0)) {
              System.out.println("Overlapaju se");
              return false;
        }
         return true;
     }
     private Boolean overlapTime(LocalTime newStart,LocalTime newEnd,
                                    LocalTime oldStart,LocalTime oldEnd){
            if(((newStart.compareTo(oldStart)>=0 && newStart.compareTo(oldEnd)<=0)||
                    (newEnd.compareTo(oldStart)>=0 && newEnd.compareTo(oldEnd)<=0)) ||
                    (newStart.compareTo(oldStart)<=0 && newEnd.compareTo(oldStart)>=0)) {
                 System.out.println("Overlapaju se");
            return false;
        }
            return true;
        }
     private Boolean filterReservation(LocalDateTime start,LocalDateTime end,Long id){
         boolean can=true;
         List<Reservation> reservations=reservationRepository.findAll();
         for(Reservation reservation:reservations){
             if(id.equals(reservation.getParkingSpot().getId_parkingSpot())){
                 if(reservation.getRepetitive()){
                     DayOfWeek dayOfWeek=reservation.getReservation_start().getDayOfWeek();
                     DayOfWeek nOfWeek=reservation.getReservation_end().getDayOfWeek();
                     if(dayOfWeek.equals(start.getDayOfWeek()) || dayOfWeek.equals(end.getDayOfWeek())
                      || nOfWeek.equals(start.getDayOfWeek()) || nOfWeek.equals(end.getDayOfWeek())){
                          if(!overlapTime(start.toLocalTime(),end.toLocalTime(), 
                              reservation.getReservation_start().toLocalTime(), reservation.getReservation_end().toLocalTime())){
                            can=false;
                            throw new RequestDeniedException("Parking spot is already taken at that time.");
                        }
                } 
            }else {
                if (!overlap(start,end,reservation.getReservation_start(),reservation.getReservation_end())){
                    can=false;
                    throw new RequestDeniedException("Parking spot is already taken at that time.");
                }
                 }
             }
         }
         return can;
     }

    public User makeReservation(User currentUser, ReservationDTO reservationDTO, Long id) {
        ParkingSpot spot= parkingSpotRepository.findById(id).get();
        LocalDateTime start = LocalDateTime.parse(reservationDTO.getReservation_start());
        LocalDateTime end = LocalDateTime.parse(reservationDTO.getReservation_end());
        boolean can=false;
       if(!reservationDTO.getRepetitive()) {
           can=filterReservation(start, end, id);
       }
        Reservation r=null;
        if(can){
            Reservation reservation= new Reservation(reservationDTO.getRepetitive(), start, end, currentUser, spot);
            if(reservationDTO.getPrice()>currentUser.getWallet()){
                throw new RequestDeniedException("You don't have enough money, please top up your wallet.");
             }
            currentUser.setWallet(currentUser.getWallet()-reservationDTO.getPrice());
            r=reservationRepository.save(reservation);
        }
        if(reservationDTO.getRepetitive()){
            List<Reservation> reservations=reservationRepository.findAll();
            for(Reservation reservation:reservations){
                if(id.equals(reservation.getParkingSpot().getId_parkingSpot())){
                    DayOfWeek dayOfWeek=reservation.getReservation_start().getDayOfWeek();
                    DayOfWeek nOfWeek=reservation.getReservation_end().getDayOfWeek();
                    if(dayOfWeek.equals(start.getDayOfWeek()) || dayOfWeek.equals(end.getDayOfWeek())
                     || nOfWeek.equals(start.getDayOfWeek()) || nOfWeek.equals(end.getDayOfWeek())) {
                        System.out.println("Prosao day of week");
                        if (!overlap(start,end,reservation.getReservation_start(),reservation.getReservation_end())){
                            can=false;
                            throw new RequestDeniedException("Parking spot is already taken at that time.");
                        }else{
                            Reservation reservationn= new Reservation(reservationDTO.getRepetitive(), start, end, currentUser, spot);
                            System.out.println("rezervacija"+reservationn.getReservation_end());
                            if(reservationDTO.getPrice()>currentUser.getWallet()){
                                throw new RequestDeniedException("You don't have enough money, please top up your wallet.");
                            }
                            currentUser.setWallet(currentUser.getWallet()-reservationDTO.getPrice());
                            r=reservationRepository.save(reservationn);
                        }
                } 
        }}
    }
        return r.getClient();
    }

    public List<Reservation> allReservations(User currentUser) {
        Long id=currentUser.getId();
        List<Reservation> reservations=reservationRepository.findAll().stream().filter(
            (r) -> r.getClient().getId()==id).toList();
        for(Reservation r:reservations){
            r.setClient(null);
            r.getParkingSpot().getParking().setOwner(null);
        }
        return reservations;
    }

    //returns a list of all reservations of a specific parking owned by a parkingowner
    public List<Reservation> getOwnersReservations(User owner) {
        Long parkingId = owner.getIdParking();

        List<ParkingSpot> parkingSpots = parkingSpotRepository.findAll().stream().filter(
                (parkingSpot) -> parkingSpot.getParking().getId_parking() == parkingId
        ).toList();

        List<Reservation> allReservations = reservationRepository.findAll();
        List<Reservation> reservations = new ArrayList<>();
        for(ParkingSpot parkingSpot : parkingSpots) {
            Long parkingSpotId = parkingSpot.getId_parkingSpot();
            for(Reservation reservation : allReservations) {
                if(parkingSpotId == reservation.getParkingSpot().getId_parkingSpot())
                    reservations.add(reservation);
            }
        }
        return reservations;
    }
    public List<Reservation> filter(ReservationsDTO reservationsDTO) {
        List<Reservation> allReservations=reservationRepository.findAll();
        List<Long> parkingSpotsid=reservationsDTO.getIds();
        List<Reservation> returnReservations=new ArrayList<>();
        LocalDate date=LocalDate.parse(reservationsDTO.getDate());
        for(Reservation reservation:allReservations){
            LocalDate start=reservation.getReservation_start().toLocalDate();
            LocalDate end=reservation.getReservation_end().toLocalDate();
            if(start.equals(date) || end.equals(date)){
                Long id=reservation.getParkingSpot().getId_parkingSpot();
                if (parkingSpotsid.contains(id)){
                    returnReservations.add(reservation);
                }
            }   
        }
        return returnReservations;
    }

    
}
