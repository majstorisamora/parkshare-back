package parkshare.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import parkshare.domain.Reservation;
import parkshare.domain.User;
import parkshare.exception.RequestDeniedException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class StatisticsService {

    @Autowired
    private ReservationService reservationService;

    //returns a list of the number of monthly reservations for each month that has passed since the first reservation
    public List<Integer> getNoOfResPerMonth(User owner) {
        List<Reservation> allReservations = reservationService.getOwnersReservations(owner);
        if (allReservations.isEmpty()) throw new RequestDeniedException("Owner doesn't have any reservations yet");
        List<Integer> noOfRes = new ArrayList<>();

        //filter all past reservations, remove future ones
        List<Reservation> reservations = new ArrayList<>();
        for(Reservation r : allReservations) {
            if(r.getReservation_start().isBefore(LocalDateTime.now()))
                reservations.add(r);
        }

        //finds the oldest reservation
        Reservation oldest = new Reservation();
        if(reservations.get(0).getReservation_start().isBefore(LocalDateTime.now()))
            oldest = reservations.get(0);
        for(Reservation r : reservations) {
            if(r.getReservation_start().isBefore(oldest.getReservation_start()) &&
                    r.getReservation_start().isBefore(LocalDateTime.now()))
                oldest = r;
        }
        //finds the number of months passed from the oldest reservation until today
        int numberOfMonths = calculateNoOfMonths(oldest);

        for(int i = 0; i < numberOfMonths + 1; i++) {
            float cnt = 0;
            float repeatingRes = 0;
            for(Reservation r : reservations) {
                LocalDateTime resStart = r.getReservation_start();
                if(calculateNoOfMonths(r) == numberOfMonths - i) {
                    cnt++;
                    if(r.getRepetitive()) {
                        if(numberOfMonths - i == 0)
                            repeatingRes += (LocalDateTime.now().getDayOfMonth() - resStart.getDayOfMonth()) / 7;
                        else
                            repeatingRes += (resStart.getMonth().length(isLeapYear(resStart.getYear())) -
                                    resStart.getDayOfMonth()) / 7;
                    }
                } else if(r.getRepetitive() && calculateNoOfMonths(r) > numberOfMonths - i) {
                    if(numberOfMonths - i == 0)
                        repeatingRes += LocalDateTime.now().getDayOfMonth() / 7;
                    else
                        repeatingRes += 4.5;
                }
            }
            cnt += repeatingRes;
            noOfRes.add(Math.round(cnt));
        }

        return noOfRes;
    }

    private int calculateNoOfMonths(Reservation reservation) {
        int yearDifference = LocalDateTime.now().getYear() - reservation.getReservation_start().getYear();
        int monthDifference = LocalDateTime.now().getMonthValue() - reservation.getReservation_start().getMonthValue();
        int numberOfMonths = 12*yearDifference + monthDifference;
        return numberOfMonths;
    }

    private boolean isLeapYear(int year) {
        if (year % 4 != 0 || year % 100 == 0) {
            return false;
        } else {
            return true;
        }
    }

}