package parkshare.service.impl;


import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import parkshare.dao.UserRepository;
import parkshare.domain.ConfirmationToken;

import parkshare.domain.User;
import parkshare.domain.UserRole;
import parkshare.exception.EntityMissingException;
import parkshare.exception.RequestDeniedException;
import parkshare.rest.AppUserDetailsService;
import parkshare.rest.DTO.UserDTO;
import parkshare.rest.DTO.WalletDTO;
import parkshare.service.ConfirmationTokenService;
import parkshare.service.JwtUtil;
import parkshare.service.Email.EmailSender;
import parkshare.service.Email.EmailValidator;
import parkshare.service.UserServiceIn;
import org.springframework.security.authentication.AuthenticationManager;



import javax.validation.UnexpectedTypeException;

@Service
public class UserService implements UserServiceIn {
    private static final String NUM_OF_CREDIT_CARD = "[0-9]{16}";
    private static final String CVC = "[0-9]{3}";
    private static final String IBAN_FORMAT="HR[0-9]{19}";

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private EmailValidator emailValidator;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConfirmationTokenService confirmationTokenService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private AppUserDetailsService appUserDetailsService;

    @Override
    public List<User> listAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findById(long userId) {
        return userRepository.findById(userId);
    }

    @Override
    public User fetch(long userId) {
        return findById(userId).orElseThrow(
                () -> new EntityMissingException(User.class, userId)
        );
    }

    @Override
    public String singUpUser(User user) {
        //provjeri jeli username unique
        boolean exists = userRepository.findByUsername(user.getUsername()).isPresent();
        if (exists) throw new RequestDeniedException("This username is already taken. Please choose another username.");
        //provjeri jeli email unique 
        exists = userRepository.findByEmail(user.getEmail()).isPresent();
        if (exists) throw new RequestDeniedException("This email is already taken. Please choose another email.");
        String encodedpass = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encodedpass);
        //ako je username admin -> stavi mu role ADMIN
        if (user.getUsername().equals("admin")) user.setUserRole(UserRole.ADMIN);
        userRepository.save(user);
        String token = UUID.randomUUID().toString();
        ConfirmationToken inftoken = new ConfirmationToken(token, LocalDateTime.now(), LocalDateTime.now().plusHours(24), user);
        confirmationTokenService.saveConfirmationToken(inftoken);
        return token;

    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User updateUser(User newUser) {
        Long id = newUser.getId();
        if (!userRepository.existsById(id))
            throw new EntityMissingException(User.class, id);
        User oldUser = userRepository.findById(id).get();
        if (!oldUser.getUsername().equals(newUser.getUsername())) {
            if (userRepository.countByUsername(newUser.getUsername()) == 1) {
                throw new RequestDeniedException("User with username " + newUser.getUsername() + " already exists");
            }
        }
        boolean goodIban=newUser.getIban().matches(IBAN_FORMAT);
        if(!goodIban){
            throw new RequestDeniedException("IBAN must start with HR and have 19 digits");
        }
        if (newUser.getNumOfCreditCard().matches(NUM_OF_CREDIT_CARD)) {
            newUser.setNumOfCreditCard(newUser.getNumOfCreditCard());
        } else {
            throw new RequestDeniedException("Entered number of credit card is not valid.");
        }

        boolean isValid=emailValidator.test(newUser.getEmail());
        if(!isValid){
            throw new RequestDeniedException("Email not valid");
        }
        if (!oldUser.getEmail().equals(newUser.getEmail())) {
            if (userRepository.countByEmail(newUser.getEmail()) == 1) {
                throw new RequestDeniedException("User with email " + newUser.getEmail() + " already exists");
            }
        }
        return userRepository.save(newUser);
    }

    @Override
    public User deleteUser(Long userId) {
        User user = fetch(userId);
        userRepository.delete(user);
        return user;
    }

    @Override
    public Optional<User> findUser(String username, String password) {

        //provjeriti jesu li uneseni ispravni podatci
        if (username == null) throw new RequestDeniedException("Username must be set,not null");
        if (password == null) throw new RequestDeniedException("Password must be set,not null");

        //provjera je li postoji korisnik s danim username-om
        try {
            Optional<User> user = userRepository.findByUsername(username);
        } catch (Exception e) {
            throw new RequestDeniedException("User with username: " + username + " does not exist");
        }
        Optional<User> user = this.userRepository.findByUsername(username);
        //provjera je li dani password odgovara passwordu u bazi
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        // if (!encoder.matches(password, user.get().getPassword())) {
        //     throw new RequestDeniedException("Entered password is not correct");
        // }
        if (user.get().getEnabled() == 0) throw new RequestDeniedException("User must confirm email first");
        //provjera je li admin potvrdio registraciju korisnika
        if (user.get().getEnabled() == 1) throw new RequestDeniedException("User must be confirmed by admin");
        
        //mora vratiti token tog user-a 
        return user;
    }

    public int enableUser(String username) {
        return userRepository.enableUser(username);
    }

    public List<User> listUnconfirmedOwners() {
        return userRepository.findAllOwnersUnconfirmedByAdmin();
    }

    public User enableUnconfirmedOwner(String username) {
        userRepository.enableUser(username);
        User user = userRepository.findByUsername(username).orElseThrow(() -> new RequestDeniedException("There is no such username")).present();
        String emailUser = user.getEmail();
        emailSender.send(emailUser, confimationEmail(username));
        return user;
    }

    public int askAdminConfirmation(String username) {
        return userRepository.askAdminConfirmation(username);
    }

    public User updateUserData(Long id, UserDTO newUser) {
        User newUser2= userRepository.findById(id).get();
        if (!newUser2.getUsername().equals(newUser.getUsername())) {
            if (userRepository.countByUsername(newUser.getUsername()) == 1) {
                throw new RequestDeniedException("User with username " + newUser.getUsername() + " already exists");
            }
        }
        if (!newUser2.getEmail().equals(newUser.getEmail())) {
            if (userRepository.countByEmail(newUser.getEmail()) == 1) {
                throw new RequestDeniedException("User with email " + newUser.getEmail() + " already exists");
            }
        }
        newUser2.setEmail(newUser.getEmail());
        newUser2.setFirstName(newUser.getFirstName());
        newUser2.setLastName(newUser.getLastName());
        newUser2.setUsername(newUser.getUsername());
        newUser2.setNumOfCreditCard(newUser.getNumOfCreditCard());
        newUser2.setIban(newUser.getIban());
        //return ResponseEntity.ok(new AuthenticationResponse(jwt));
        return updateUser(newUser2);
    }

    public List<User> presentAll() {
        return listAll().stream().map(user -> user.present()).toList();
    }

    public User presentByUsername(String username) {
        return findByUsername(username).orElseThrow(() -> new UnexpectedTypeException("Unexpected exception")).present();
    }

    public User presentById(Long id) {
        return findById(id).orElseThrow(() -> new UnexpectedTypeException("Unexpected exception")).present();
    }

    //izbacen parametar WalletDTO dto
    public User activateWallet(User user) {
        //provjera jesu li korisniku dodjeljena prava klijenta
        if (user.getUserRole().equals(UserRole.CLIENT)) {
            /*
            //provjera jesu li uneseni podatci ispravni
            if (dto.getNumOfCreditCard().matches(NUM_OF_CREDIT_CARD)) {
                user.setNumOfCreditCard(dto.getNumOfCreditCard());
            } else {
                throw new RequestDeniedException("Entered number of credit card is not valid.");
            }
            if (!dto.getCvc().matches(CVC)) {
                throw new RequestDeniedException("Entered CVC is not valid.");
            }
            if (dto.getExpirationDate().isEmpty()) {
                throw new RequestDeniedException("Expiration date of credit card must be set, not null");
            }
            String[] currDate = LocalDate.now().toString().split("-");
            String[] expDate = dto.getExpirationDate().split("/");
            currDate[0] = currDate[0].substring(1, currDate[0].length());

            //ako kartica vise ne vrijedi
            if ((Integer.parseInt(currDate[0]) >= Integer.parseInt(expDate[1]) &&
                    Integer.parseInt(currDate[1]) >= Integer.parseInt(expDate[0]))) {
                throw new RequestDeniedException("Entered expiration date is not valid");
            }
             */
            if (!user.getWallet_enabled()) {
                //postaviti da je aktiviran novcanik
                user.setWallet_enabled(true);
                //postaviti pocetnu vrijednost novcanika na 50
                user.setWallet(Float.parseFloat("50"));
            /*
            //spremiti broj kreditne kartice u bazu podataka
            user.setNumOfCreditCard(dto.getNumOfCreditCard());
             */
                return this.userRepository.save(user);
            } else {
                throw new RequestDeniedException("User with username " + user.getUsername() + " has already activated wallet");
            }
        } else {
            throw new RequestDeniedException("User with username " + user.getUsername() + " cannot activate wallet");
        }
    }
    /*
    public void updateWallet(User user, WalletDTO dto) {
        //provjera je li korisnik ima prava klijenta i je li mu aktiviran novcanik
        if (user.getUserRole().equals(UserRole.CLIENT) && user.getWallet_enabled()) {

            //provjera jesu li uneseni podatci ispravni
            if (dto.getNumOfCreditCard().matches(NUM_OF_CREDIT_CARD)) {
                user.setNumOfCreditCard(dto.getNumOfCreditCard());
            } else {
                throw new RequestDeniedException("Entered number of credit card is not valid.");
            }
            if (!dto.getCvc().matches(CVC)) {
                throw new RequestDeniedException("Entered CVC is not valid.");
            }
            if (dto.getExpirationDate().isEmpty()) {
                throw new RequestDeniedException("Expiration date of credit card must be set, not null");
            }
            String[] currDate = LocalDate.now().toString().split("-");
            String[] expDate = dto.getExpirationDate().split("/");
            currDate[0] = currDate[0].substring(1, currDate[0].length());

            //ako kartica vise ne vrijedi
            if ((Integer.parseInt(currDate[0]) >= Integer.parseInt(expDate[1]) &&
                    Integer.parseInt(currDate[1]) >= Integer.parseInt(expDate[0]))) {
                throw new RequestDeniedException("Entered expiration date is not valid");
            }
            //ako su svi podatci ispravni, potrebno je spremiti novi broj kreditne kartice u bazu
            this.userRepository.save(user);
        } else {
            throw new RequestDeniedException("User with username " + user.getUsername() + " cannot change datas of wallet");
        }
    }
    */
    public Float checkWalletStatus(User user) {
        //ako korisnik ima ulogu klijenta i ako ima aktiviran novcanik
        if (user.getUserRole().equals(UserRole.CLIENT) && user.getWallet_enabled()) {
            return user.getWallet();
        } else {
            throw new RequestDeniedException("User with username: " + user.getUsername() + " cannot check wallet status");
        }
    }

    public User depositInWallet(User user, WalletDTO dto) {
        //ako je novcanik aktiviran
        if (user.getWallet_enabled()) {
            //provjera jesu li uneseni podatci ispravni
            if (dto.getNumOfCreditCard().matches(NUM_OF_CREDIT_CARD)) {
                user.setNumOfCreditCard(dto.getNumOfCreditCard());
            } else {
                throw new RequestDeniedException("Entered number of credit card is not valid.");
            }
            user.setWallet(user.getWallet() + dto.getAmount());
            return userRepository.save(user);
        } else {
            throw new RequestDeniedException("User with username: " + user.getUsername() + " doesnt have enabled wallet");
        }
    }

    public void payFromWallet(User user, Float amount){
        if (user.getWallet_enabled()) {
            if(user.getWallet() - amount < 0){
                throw new RequestDeniedException("Not enough credit in wallet");
            }
            user.setWallet(user.getWallet()-amount);
            userRepository.save(user);
        } else {
            throw new RequestDeniedException("User with username: " + user.getUsername() + " cdoesnt have enabled wallet");
        }
    }
    public String confimationEmail(String username) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Your new ParkShare password</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi, " + username + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">ParkShare Administrator has confirmed you as a Parking Owner</p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">You can now enter your own parking and more</p></blockquote>\n <p>We're looking forward to cooperating with you</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }

}