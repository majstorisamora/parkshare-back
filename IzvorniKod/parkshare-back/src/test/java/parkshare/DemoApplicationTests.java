package parkshare;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {

	@BeforeAll
	public static void setup(){
		System.out.println("Uspotavljanje veze s DB");
	}
	@Test
	void contextLoads() {
	}

	@AfterAll
	public static void tearDown(){
		System.out.println("Zatvaranje veze s DB");
	}
}
