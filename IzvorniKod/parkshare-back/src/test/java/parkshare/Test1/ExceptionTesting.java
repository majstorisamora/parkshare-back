package parkshare.Test1;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import parkshare.dao.UserRepository;
import parkshare.domain.User;
import parkshare.exception.RequestDeniedException;
import parkshare.service.impl.UserService;

@SpringBootTest
public class ExceptionTesting {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;
    @Test
    public void testException(){
        User user=userRepository.findByUsername("Dora").get();
        user.setIban("HK123456879");
        Throwable exception= 
        assertThrows(RequestDeniedException.class,
        ()-> userService.updateUser(user));
        assertEquals("IBAN must start with HR and have 19 digits",
        exception.getMessage());
    }
    
}
