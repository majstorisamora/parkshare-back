package parkshare.Test2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import parkshare.dao.ParkingRepository;
import parkshare.dao.ParkingSpotsRepository;
import parkshare.dao.ReservationRepository;
import parkshare.dao.UserRepository;
import parkshare.domain.User;
import parkshare.domain.UserRole;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import java.lang.Iterable;


@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=AutoConfigureTestDatabase.Replace.NONE)
public class RepositoryTest {
    @Autowired
    private UserRepository userRepository;
     @Autowired
     private ReservationRepository reservationRepository;
     @Autowired 
     private ParkingRepository parkingRepository;
     @Autowired
     private ParkingSpotsRepository parkingSpotsRepository;
   

    @Test
    void testCreateAndDelete() {
        reservationRepository.deleteAll();
        parkingSpotsRepository.deleteAll();
        parkingRepository.deleteAll();
        parkingRepository.deleteAll();
        userRepository.deleteAll();
    
        User user1=new User("Test", "Test", "Test", "Test", "test@gmail.com",
        null, "HR1234567891234567891", UserRole.PARKINGOWNER, null, false, null, null, null);
        userRepository.save(user1);
        Iterable<User> users=userRepository.findAll();
        Assertions.assertThat(users).extracting(User::getFirstName).containsOnly("Test");
        
        userRepository.deleteAll();
        Assertions.assertThat(userRepository.findAll().isEmpty());
    }

}
