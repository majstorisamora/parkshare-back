package parkshare.Test3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import parkshare.dao.UserRepository;
import parkshare.domain.User;
import parkshare.service.impl.UserService;

@ExtendWith(MockitoExtension.class)
public class ServiceTest {

    @InjectMocks
    UserService service;

    @Mock
    UserRepository dao;

    @Test
    public void testFindAllUsers(){
        List<User> list=new ArrayList<User>();
        User user1=new User("Dora", "Dora", "Dora", "Dora", "app.p.arkshare@gmail.com",
                             null, "HR1234567891234567891", null, null, null, null, null, null);
        User user2=new User("Marin", "Marin", "Marin", "Marin", "app.pa.rkshare@gmail.com",
                             null, "HR1234567891234567891", null, null, null, null, null, null);
        User user3=new User("Neksi", "Neksi", "Neksi", "Neksi", "app.par.kshare@gmail.com",
                             null, "HR1234567891234567891", null, null, null, null, null, null);
        list.add(user1);
        list.add(user2);
        list.add(user3);

        when(dao.findAll()).thenReturn(list);

        //test
        List<User> userList=service.listAll();

        assertEquals(3,userList.size());
        verify(dao,times(1)).findAll();
        }
}
