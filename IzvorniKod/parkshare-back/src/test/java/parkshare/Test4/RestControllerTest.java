package parkshare.Test4;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import parkshare.domain.Parking;
import parkshare.domain.User;
import parkshare.rest.AppUserDetailsService;
import parkshare.rest.CORSFilter;
import parkshare.rest.JwtRequestFilter;
import parkshare.rest.ParkingController;
import parkshare.rest.WebSecurityConfig;
import parkshare.service.JwtUtil;
import parkshare.service.ParkingService;
import parkshare.service.impl.UserService;

import java.util.List;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@ContextConfiguration(classes = ParkingController.class)
@Import(WebSecurityConfig.class)
@AutoConfigureMockMvc(addFilters = false)

public class RestControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    ParkingService parkingService;
    @MockBean
    UserService userService;
    @MockBean
    private JwtUtil jwtUtil;
    @MockBean
    private AppUserDetailsService appUserDetailsService;
    @MockBean
    private  BCryptPasswordEncoder bCryptPasswordEncoder;
    @MockBean
    private JwtRequestFilter jwtRequestFilter;
    @MockBean
    private CORSFilter corsFilter;

    @Test
    public void testFindAll() throws Exception{
        User user1=new User("Dora", "Dora", "Dora", "Dora", "app.p.arkshare@gmail.com",
        null, "HR1234567891234567891", null, null, null, null, null, null);
        Parking parking= new Parking("Dora parking", "description", 10.0f, "type", null, 0, user1, "location");
        List<Parking> parkings=java.util.Arrays.asList(parking);

        Mockito.when(parkingService.findAll()).thenReturn(parkings);

        mockMvc.perform(get("/parking/all"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$",Matchers.hasSize(1)))
        .andExpect(jsonPath("$[0].name",Matchers.is("Dora parking")));
    }
    
}
