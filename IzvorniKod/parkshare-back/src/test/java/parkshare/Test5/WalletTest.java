package parkshare.Test5;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import parkshare.dao.UserRepository;
import parkshare.domain.User;
import parkshare.domain.UserRole;
import parkshare.rest.DTO.WalletDTO;
import parkshare.service.impl.UserService;

@ExtendWith(MockitoExtension.class)
public class WalletTest {

    @InjectMocks
    UserService userService;

    @Mock
    UserRepository userRepository;
    
    @Test
    public void testDepositWallet(){
        User user1=new User("Test", "Test", "Test", "Test", "test@gmail.com",
        null, "HR1234567891234567891", UserRole.CLIENT, null, false, null, null, null);
        
        userService.activateWallet(user1);

        WalletDTO walletDTO= new WalletDTO("1234567891234567", 50.00f);
        userService.depositInWallet(user1,walletDTO);

        assertEquals(100.00f, user1.getWallet());
    }
}
