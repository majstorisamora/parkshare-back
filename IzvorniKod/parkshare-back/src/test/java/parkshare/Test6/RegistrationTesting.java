package parkshare.Test6;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import parkshare.dao.UserRepository;
import parkshare.domain.UserRole;
import parkshare.exception.RequestDeniedException;
import parkshare.rest.DTO.RegistrationDTO;
import parkshare.service.RegistrationService;

@SpringBootTest
public class RegistrationTesting {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RegistrationService registrationService;

    @Test
    public void testRegistration(){
        RegistrationDTO dto=new RegistrationDTO("Dora", "Dora", "app.parkshare@gmail.com", 
                     "Dora", "Dora", "HR1234567891234567891",UserRole.CLIENT, null);
        Throwable exception=
        assertThrows(RequestDeniedException.class,()-> registrationService.register(dto));
        assertEquals("This username is already taken. Please choose another username.",exception.getMessage());

        dto.setUsername("NewUsername");
        exception= assertThrows(RequestDeniedException.class,()-> registrationService.register(dto));
        assertEquals("This email is already taken. Please choose another email.",exception.getMessage());

    }
}
