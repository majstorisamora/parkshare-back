# Koraci za pokrenuti projekt lokalno i povezati se na [ParkShare Front](https://gitlab.com/majstorisamora/parkshare-front)
1. U mapi `/IzvorniKod/parkshare-back/` pokrenuti `./mvnw clean install` i to instalira sve potrebne module za projekt.
2. Unutar `application.properties` postaviti username i šifru lokalne postgres baze
3. Također pokrenuti `./mvnw spring-boot:run`. Ovo će pokrenuti Spring Boot aplikaciju na konfiguriranom portu 8080
